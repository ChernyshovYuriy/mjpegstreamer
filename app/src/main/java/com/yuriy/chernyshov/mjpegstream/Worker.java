package com.yuriy.chernyshov.mjpegstream;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 23.10.14
 * Time: 20:08
 */
public class Worker extends Thread {

    private static final String LOG_TAG = Worker.class.getSimpleName();

    private static final String BOUNDARY_START = "--myboundary\r\nContent-type: image/jpg\r\nContent-Length: ";
    private static final String BOUNDARY_END = "\r\n\r\n";

    private File mMJPEGFile;
    private FileOutputStream mFileOutputStream;

    private View mView;
    private volatile boolean mDoStop;

    public void setView(View value) {
        mView = value;
        mView.setDrawingCacheEnabled(true);
    }

    public void onStop() {
        mDoStop = true;
    }

    @Override
    public void run() {
        super.run();

        mDoStop = false;
        Bitmap bitmap;
        final ByteArrayOutputStream jpegByteArrayOutputStream = new ByteArrayOutputStream();
        try {
            mMJPEGFile = File.createTempFile("videocapture", ".mjpeg",
                    Environment.getExternalStorageDirectory());
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException:" + e.getMessage());
        }

        try {
            mFileOutputStream = new FileOutputStream(mMJPEGFile);
        } catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "FileNotFoundException:" + e.getMessage());
        }

        final BufferedOutputStream bufferedOutputStream
                = new BufferedOutputStream(mFileOutputStream);

        while (!mDoStop) {

            bitmap = mView.getDrawingCache();
            try {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, jpegByteArrayOutputStream);

                byte[] jpegByteArray = jpegByteArrayOutputStream.toByteArray();

                byte[] boundaryBytes =
                        (BOUNDARY_START + jpegByteArray.length + BOUNDARY_END).getBytes();
                bufferedOutputStream.write(boundaryBytes);

                bufferedOutputStream.write(jpegByteArray);
                bufferedOutputStream.flush();
            } catch (IOException e) {
                Log.e(LOG_TAG, "IOException:" + e.getMessage());
            }

            Log.d(LOG_TAG, "Bitmap:" + jpegByteArrayOutputStream.toByteArray().length);
        }

        try {
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException:" + e.getMessage());
        }
    }
}