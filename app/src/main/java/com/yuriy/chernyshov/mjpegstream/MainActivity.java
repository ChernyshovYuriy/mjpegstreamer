package com.yuriy.chernyshov.mjpegstream;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    private Worker mWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

    }

    public void onStart(View view) {
        mWorker = new Worker();

        mWorker.setView(findViewById(R.id.main_view));

        mWorker.start();
    }

    public void onStop(View view) {
        mWorker.onStop();
    }
}
